
//for wifi
#include <ESP8266WiFi.h>

//upload bin file online
#include <ESP8266WebServer.h> //include the webserver library
#include <ESP8266HTTPUpdateServer.h> // **** include the OTA updater library

#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>
//library to display to lcd
#include <LiquidCrystal_I2C.h>

int hostId = 192;

//Replace with your networks credentials
const char *ssid = "Wifi_Er";
const char *password = "wmdcwifier";

LiquidCrystal_I2C lcd(0x27,16,2);
ESP8266WebServer httpServer(80); // create a webserver object
ESP8266HTTPUpdateServer httpUpdater; //**** create an updater object

HTTPClient http;

void setup() {
  // put your setup code here, to run once:

  //Initialize Serial Monitor
  Serial.begin(115200);
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  IPAddress ip(192,168,1,hostId);
  IPAddress gateway(192,168,1,100);
  IPAddress subnet(255,255,255,0);
  IPAddress dns(192,168,1,100);
  WiFi.config(ip, gateway, subnet, dns);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("IP address");
    Serial.print(WiFi.localIP());
    Serial.print(".");
    
  }

  httpServer.on("/", indexHTML);  // specify function to call when client browses to root
  httpServer.begin();             // start the webserver
  httpUpdater.setup(&httpServer);
  
  lcd.init();
  lcd.backlight();
  
  lcd.setCursor(0,0);
  lcd.print("Hello Arduino"); // change it to  lcd.print("Hello World"); then compile and look
                            // for the .bin file in folder Appdata/Local/Temp/"your folder"
                            // then uplaod the file in seacrh your IPAddress in my case my ip
                            // is 192.168.1.192/update   put/update
}

void indexHTML(){
     String msg = "<html>Welcome To Arduino</html>";
     msg += "<br>Click here <a href=\"/update\">" + WiFi.localIP().toString() + "/update</a> to update firmware.";
     httpServer.send(200, "text/html", msg);
     
}

void loop() {
  // put your main code here, to run repeatedly:
  httpServer.handleClient();
}
